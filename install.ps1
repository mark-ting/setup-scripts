﻿# install.ps1
# 
# Install Chocolatey and run a pre-configured setup script.


# Install Chocolatey
iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

# Run setup script
$ScriptPath = Split-Path $MyInvocation.InvocationName
& "$ScriptPath\setup.ps1"