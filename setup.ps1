﻿# setup.ps1
#
# Setup script for applications to install via choco install.


# Allow script to run unattended
choco feature enable -n=allowGlobalConfirmation
choco feature enable -n=allowEmptyChecksums

# Web Browsers
cinst firefox
cinst googlechrome

# Utilities
cinst 7zip.install
cinst dropbox
cinst everything
cinst greenshot
cinst nircmd
cinst teamviewer
cinst treesizefree

# Dev Tools
cinst jdk8
cinst nodejs.install
cinst mysql.workbench
cinst poshgit
cinst putty.install
cinst vagrant
cinst virtualbox
cinst winscp.install

# Code Editors
cinst notepadplusplus.install
cinst visualstudiocode

# Communication
cinst discord
cinst skype
cinst teamspeak

# Multimedia
cinst spotify
cinst streamlink-twitch-gui
cinst vlc

# Uncategorized
cinst gimp
cinst inkscape
cinst paint.net
cinst sumatrapdf


# Upgrade stage (re-run/post-run)
cup

# Disable confirmation to improve security
choco feature disable -n=allowGlobalConfirmation
choco feature disable -n=allowEmptyChecksums

# Wait for input before closing.
if ($Host.Name -eq "ConsoleHost")
{
    Write-Host "Setup Complete. Press any key to continue..."
    $Host.UI.RawUI.FlushInputBuffer()
    $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp") > $null
}